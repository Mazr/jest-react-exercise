import React from 'react';

import '@testing-library/jest-dom/extend-expect';
import '@testing-library/react/cleanup-after-each';

import { render, fireEvent } from '@testing-library/react';

import axios from 'axios';
import 'babel-polyfill';

import OrderComponent from '../Order';

jest.mock('axios'); // Mock axios模块

test('Order组件显示异步调用订单数据', async () => {
  // <--start
  // TODO 4: 给出正确的测试
  // setup组件
  const { findByTestId, findByLabelText, getByLabelText } = render(<OrderComponent />);
  const input = await findByLabelText('number-input');
  const button = getByLabelText('submit-button');
  const orderStatus = await findByTestId('status');
  // Mock数据请求
  const resp = { data: { status: '???' } };
  await axios.get.mockResolvedValue(resp);
  // axiosMock.get.mockResolvedValueOnce({ data: { greeting: "hello there" } });

  // 触发事件
  await fireEvent.change(input, { target: { value: '123' } });
  await fireEvent.click(button);
  // 给出断言
  await expect(input).toHaveValue('123');
  await expect(orderStatus).toHaveTextContent('???');
  // --end->
});
