import React from 'react';

import '@testing-library/jest-dom/extend-expect';
import '@testing-library/react/cleanup-after-each';

import { render } from '@testing-library/react';
import TitleWithState from '../TitleWithState';

// <--start
// TODO: 少什么就引点儿什么吧。
// --end->

test('TitleWithState组件渲染内容', () => {
  // <--start
  // TODO 2: 给出正确的assertion，测试Title组件渲染内容
  const { getByTestId } = render(<TitleWithState />);
  expect(getByTestId('title')).toHaveTextContent('Silent World');
  // --end->
});
